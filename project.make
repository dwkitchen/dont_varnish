api = 2
core = 7.x

includes[] = http://cgit.drupalcode.org/ee/plain/drupal-org-core.make?h=7.x-1.x

; Profile ===========================================================================

projects[ee][type] = profile
projects[ee][subdir] = ""
projects[ee][download][type] = git
projects[ee][download][url] = http://git.drupal.org/project/ee.git
projects[ee][download][branch] = 7.x-1.x

projects[ee_commerce][type] = module
projects[ee_commerce][download][type] = git
projects[ee_commerce][download][url] = http://git.drupal.org/project/ee_commerce.git
projects[ee_commerce][download][branch] = 7.x-1.x

; ===================================================================================

; Drush make allows a default sub directory for all contributed projects.
defaults[projects][subdir] = contrib

; Platform indicator module.
projects[platform][version] = 1.3

; Features

projects[features][version] = 2.6
projects[features][patch][] = http://drupal.org/files/issues/features-fix-modules-enabled-2143765-1.patch
projects[features][patch][] = http://drupal.org/files/issues/2134279-1-fix-stale-revert.patch
projects[commerce_features][version] = 1.1
projects[features_extra][version] = 1.0-beta1
projects[strongarm][version] = 2.0
projects[uuid_features][version] = 1.0-alpha4

; Development

projects[devel][version] = 1.3
projects[devel][subdir] = development
projects[commerce_devel][version] = 1.x-dev
projects[commerce_devel][subdir] = development
projects[devel_image_provider][version] = 1.x-dev
projects[devel_image_provider][subdir] = development
projects[diff][version] = 3.x
projects[diff][subdir] = development
projects[features_diff][version] = 1.0-beta1
projects[features_diff][subdir] = development
projects[blazemeter][version] = 1.0-beta6
projects[blazemeter][subdir] = development
